require "../../command"
require "../../data_types"
require "file_utils"

class DetailsModel < Optarg::Model
  string %w(-i --index), default: nil
  bool %w(-v --verbose), default: false
end

class OM::Commands::Outcome::Details < OM::Command
  @name = "details"
  @alias = %w(d detail show s)
  @use = "Shows the details of a particular outcome"

  def show_details(n : Int? = nil, show_progressions : Bool = false) : Nil
    outcomes = OM::Common.get_outcomes

    if n.nil?
      while true
        if outcomes.size == 0
          break
        end
        puts ""
        puts "Outcomes".colorize.mode(:bold).to_s
        puts ""
        output = [] of String | Array(String)
        output = output.concat print_elements outcomes
        puts align output, delimeter: ""
        puts ""
        n = "n".colorize.mode(:bold).to_s
        print "Which outcome would you like to view? [#{n}one] "
        parent = gets.as(String).strip
        if parent == "n" || parent == "q"
          exit 0
        end
        if !parent.nil?
          parent_index = parent.strip.to_i do
            0
          end
          if parent_index != 0
            outcomes = OM::Common.get_outcomes(from_outcome: "", flat: true)
            outcome = outcomes[parent_index - 1]?
            if outcome
              break
            end
            puts "\n\nPlease enter a valid number or 'n' to chose none ".colorize(:red).mode(:bold).to_s
          end
        end
      end
    else
      outcomes = OM::Common.get_outcomes(from_outcome: "", flat: true)
      outcome = outcomes[n - 1]?
    end

    if outcome.nil?
      puts "No outcome found at that index".colorize(:red).to_s
      exit 1
    end
    output = [] of String | Array(String)
    output << [""]
    output << [""]
    output << ["Outcome #{outcome.get_sha[0, 7]} | " + outcome.name.colorize(:yellow).mode(:bold).to_s]
    creation_date = outcome.date_created.to_s "%c"
    output << ["Created #{creation_date}\n"]
    output << [""]
    output << ["Description".colorize.mode(:underline).to_s]
    output << ["#{outcome.description}"]
    output << [""]
    step = OM::Common.get_head.step
    progression = "No"
    if !step.nil?
      if step.outcomes.includes? outcome.get_sha
        progression = "Yes"
      end
    end
    if show_progressions
      progressions_table = [] of String | Array(String)
      progressions_table << ["Progressions".colorize.mode(:underline).to_s]
      progressions_table << ["Sha", "Duration", "Name"]
      progressions_table << [""]
      outcome.steps.each do |progression|
        object = OM::Common.get_object progression
        p = OM::Types::Progression.from_msgpack object
        duration = Time::Span.new(seconds: p.time_taken.as(Int64), nanoseconds: 0).to_s
        progressions_table << [progression[0, 7], duration, p.title]
      end
      if progression == "Yes" && step
        step_time = Time.utc(seconds: step.time_start, nanoseconds: 0).to_s "%c"
        progressions_table << ["Current", "#{step.title} | Started #{step_time}"]
      end
      output << [OM::Common.align progressions_table, delimeter: ""]
      output << [""]
    end
    stats_table = [] of String | Array(String)
    stats_table << ["Stats".colorize.mode(:underline).to_s]
    stats_table << ["Total Completed Progressions", "#{outcome.steps.size}"]
    stats_table << ["Progression In Progress", "#{progression}"]
    output << [OM::Common.align stats_table, delimeter: ""]
    puts OM::Common.align output, delimeter: ""
  end

  def invoke(argv : Array(String)) : Nil
    result = DetailsModel.parse argv
    n = result.i? || result.index?
    if !n.nil?
      n = n.strip.to_i do
        nil
      end
    end

    show_progressions = result.v? || result.verbose?
    if show_progressions.nil?
      show_progressions = false
    end

    self.show_details n, show_progressions
  end
end
