require "./parent_command"

class MainModel < Optarg::Model
  string %w(-d --dir), default: nil, stop: true
end

class OM::Main < OM::ParentCommand
  @not_found_message = "Command not found."
  @needs_init = false

  def run(argv : Array(String))
    begin
      result = MainModel.parse argv
      dir = result.d? || result.dir?
      if dir.nil?
        dir = ENV.fetch "OM_DIR", "./.om"
      else
        argv.shift
        argv.shift
      end
    rescue
      dir = ENV.fetch "OM_DIR", "./.om"
    end

    ENV["OM_DIR"] = dir
    self.invoke argv
  end
end
