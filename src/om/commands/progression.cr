require "../parent_command"
require "./progressions/*"

class OM::Commands::Progressions < OM::ParentCommand
  @name = "progression"
  @alias = %w(progressions p)
  @use = "Start, stop and check the status of a progression"
  @default_command = "status"
end

module OM::Commands::Progression
  extend self

  def progression_information : Array(String | Array(String))
    output = [] of String | Array(String)
    # Get information from the head
    head = get_head
    # Get the step content
    step = head.step
    if step
      # Display current step (if applicable)
      step_time = Time.utc(seconds: step.time_start, nanoseconds: 0).to_s "%c"
      output << [step.title.colorize(:yellow).mode(:bold).to_s]
      output << ["Started #{step_time}\n"]

      # Get outcome from progression
      output << ["Outcomes".colorize.mode(:underline).to_s]
      step.outcomes.each { |step_outcome|
        outcome = OM::Types::Outcome.from_msgpack get_object(step_outcome)
        outcome_time = outcome.date_created.to_s "%c"
        # Display outcome
        output << ["#{step_outcome[0, 7]}", "#{outcome.name}"]
      }
    else
      output << ["Nothing Progressing"]
    end

    output
  end
end
