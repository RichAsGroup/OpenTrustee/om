require "dir"
require "./om/main"
require "./om/commands/*"

commands = [] of OM::Command

commands << OM::Commands::Init.new
commands << OM::Commands::Status.new

outcome_commands = [] of OM::Command
outcome_commands << OM::Commands::Outcome::List.new
outcome_commands << OM::Commands::Outcome::Add.new
outcome_commands << OM::Commands::Outcome::Details.new
commands << OM::Commands::Outcomes.new outcome_commands

progression_commands = [] of OM::Command
progression_commands << OM::Commands::Progression::Start.new
progression_commands << OM::Commands::Progression::Finish.new
progression_commands << OM::Commands::Progression::Abort.new
# Alias progression commands to main cmd
commands = commands + progression_commands
# Add progression status after so we don't overide normal status
progression_commands << OM::Commands::Progression::Status.new
commands << OM::Commands::Progressions.new progression_commands

commands << OM::Commands::Version.new
commands << OM::Commands::Help.new commands

om = OM::Main.new commands
om.run ARGV
