require "../../commands"
require "../../common"
require "../../data_types"
require "../progression"
require "./status"
require "optarg"
require "time"
require "colorize"

include OM::Common
include OM::Commands::Progression

class StatusModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Progression::Status < OM::Command
  @name = "status"
  @alias = %w(s)
  @use = "Display current status of project"

  def initialize : Nil
  end

  def invoke(argv : Array(String), command = self) : Nil
    result = StartModel.parse argv
    if result.h? || result.help?
      help
      exit 0
    end

    # Display project
    puts "\n"
    puts "Progression Status \n\n".colorize.mode(:bold).mode(:underline).to_s
    output = progression_information
    puts align(output, delimeter = "  ")
  end

  def help : Nil
    puts "Display status of project"
  end
end
