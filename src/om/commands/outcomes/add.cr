require "../../command"
require "../../data_types"
require "../../templates/OUTCOME_MSG"
require "math"
require "file_utils"

OUTCOME_MESSAGE_FILENAME = "OUTCOME_MSG"

class OM::Commands::Outcome::Add < OM::Command
  @name = "add"
  @alias = %w(a create)
  @use = "adds an outcome to the project"

  def add_outcome(top = true, parent : String | Nil = nil) : Nil
    # Add outcome to directory
    filename = "#{OM::Common.find_project_folder}/#{OUTCOME_MESSAGE_FILENAME}"
    File.write filename, OM::Templates.outcome_msg
    while true
      status_code, contents = OM::Common.launch_editor(filename)
      if contents == ""
        print "No Outcome Information Provided [Enter To Continue]".colorize(:red).mode(:bold).to_s
        gets
        next
      end
      contents_array = [] of String
      contents.each_line do |line|
        line = line.strip
        if line == "" || line[0] == '#'
          next
        end

        contents_array << line
      end

      if contents_array.size < 2
        print "Please provide a title and description! [Enter To Continue]".colorize(:red).mode(:bold).to_s
        gets
        next
      end

      title_length = Math.min(50, contents_array[0].size)
      title = contents_array.shift[0, title_length]
      description = contents_array.join("\n").strip
      FileUtils.rm(filename)
      break
    end

    parents = [] of String
    outcomes = OM::Common.get_outcomes

    while true
      if outcomes.size == 0
        break
      end
      puts ""
      puts "Outcomes".colorize.mode(:bold).to_s
      puts ""
      output = [] of String | Array(String)
      output = output.concat print_elements outcomes
      puts align output, delimeter: ""
      parents = parents.clear
      puts ""
      n = "n".colorize.mode(:bold).to_s
      print "Are you creating a child outcome? [#{n}one] "
      parent = gets.as(String).strip
      if parent == "n" || parent == "q"
        break
      end
      if !parent.nil?
        parent_index = parent.strip.to_i do
          0
        end
        if parent_index != 0
          flatcomes = OM::Common.get_outcomes(from_outcome: "", flat: true)
          parent_outcome = flatcomes[parent_index - 1]?
          if parent_outcome
            parents = parent_outcome.parents
            parents << parent_outcome.name
          end
          break
        else
          puts "\n\nPlease enter a valid number or 'n' to chose none ".colorize(:red).mode(:bold).to_s
        end
      end
    end

    OM::Common.create_outcome(title, description, parents)
    puts "Outcome Created!"
  end

  def invoke(argv : Array(String)) : Nil
    self.add_outcome
  end
end
