require "time"
require "msgpack"
require "yaml"

module OM::Types
  alias OutcomeTree = OM::Types::Outcome | Array(OM::Types::Outcome)
  alias NestedStringArray = String | Array(OM::Types::NestedStringArray)
end

class OM::Types::Outcome
  property name, description, parents, date_created, steps, attributes, children
  @children = [] of OM::Types::Outcome

  def initialize(@name : String, @description : String, @parents : Array(String) = [] of String,
                 @date_created : Time = Time.utc, @steps : Array(String) = [] of String,
                 @attributes : MessagePack::Table = MessagePack::Table.new)
  end

  def get_filename
    filename = @parents
    filename << @name
    File.join filename
  end

  def get_sha
    hash = OM::Common.compute_hash "outcome", String.new self.to_msgpack
    hash
  end

  MessagePack.mapping({
    name:         String,
    description:  String,
    parents:      Array(String), # Order top level parent -> immediate parent
    date_created: {type: Time, converter: Time::Format.new("%sZ%z")},
    steps:        Array(String),
    attributes:   MessagePack::Table,
  })
end

class OM::Types::Tree
  property objects

  def initialize(@objects : Hash(String, OM::Types::TreeObject))
  end

  MessagePack.mapping({
    "objects" => Hash(String, OM::Types::TreeObject),
    "parent"  => {type: String, nilable: true},
  })
end

class OM::Types::TreeObject
  def initialize(@type : String, @sha : String)
  end

  MessagePack.mapping({
    "type" => String,
    "sha"  => String,
  })
end

class OM::Types::Head
  include YAML::Serializable

  @[YAML::Field(emit_null: true)]
  property step : OM::Types::Progression?

  @[YAML::Field()]
  property tree : String

  def initialize(@step : OM::Types::Progression? = nil, @tree : String = "")
  end
end

class OM::Types::Progression
  include YAML::Serializable
  property outcomes : Array(String), title : String, time_start : Int64

  @[YAML::Field(emit_null: true)]
  property notes : String?

  @[YAML::Field(emit_null: true)]
  property time_taken : Int64?

  def initialize(@outcomes : Array(String), @title : String, @time_start : Int64 = Time.utc.to_unix,
                 @notes : String? = nil, @time_taken : Int64? = nil)
  end

  def initialize(outcome : String, @title : String, @time_start : Int64 = Time.utc.to_unix,
                 @notes : String? = nil, @time_taken : Int64? = nil)
    @outcomes = [outcome]
  end

  MessagePack.mapping({
    outcomes:   {type: Array(String)},
    title:      String,
    notes:      {type: String, nilable: true},
    time_start: Int64,
    time_taken: {type: Int64, nilable: true},
  })
end

class OM::Types::Config
  include YAML::Serializable

  property remotes : Hash(String, String)

  def initialize
    @remotes = {} of String => String
  end
end
