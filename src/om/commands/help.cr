require "../command"
require "../common"
require "colorize"

include OM::Common

class OM::Commands::Help < OM::Command
  @name = "help"
  @use = "Shows help for a specific command"
  @alias = %w(h)
  @needs_init = false

  def initialize(@commands : Array(OM::Command))
  end

  def invoke(command = self)
    command_list = @commands.map do |command|
      ["#{command.name}".colorize(:magenta).to_s, command.use]
    end
    puts "\n"
    puts align(command_list)
  end
end
