require "../../command"
require "../../common"
require "optarg"

class StartModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Progression::Start < OM::Command
  @name = "start"
  @alias = %w(begin st b)
  @use = "Begin working on a portion of an outcome"

  def initialize
  end

  def invoke(argv : Array(String), command = self)
    result = StartModel.parse argv
    if result.h? || result.help?
      help
      exit 0
    end

    # Check that at least one outcome exists
    head = OM::Common.get_head

    # Check if user is currently completing step
    if head.step
      puts "Finish your current progression!"
      return
    end

    # Get the list of outcomes
    outcomes = OM::Common.get_outcomes
    flatcomes = OM::Common.get_outcomes "", flat: true
    if outcomes.size == 0
      puts "There are no outcomes"
      return
    end

    # Ask for title, desc of step
    print "Progression Title: "
    title = gets.as(String).strip.chomp
    puts "\n\n"
    outcome_list = [] of OM::Types::Outcome
    while true
      result = OM::Common.print_elements outcomes, 0, 0, outcome_list
      puts OM::Common.align result, delimeter: ""
      print "Which outcome would you like work on (Enter nothing to continue)? "
      input = gets.as(String)

      if input == ""
        break
      elsif input == "q"
        return
      end
      i = input.to_i
      outcome = flatcomes[input.to_i - 1]?
      if !outcome
        puts "Invalid outcome"
        next
      end

      if outcome_list.includes? outcome
        outcome_list.delete outcome
        next
      end

      outcome_list << outcome
    end

    # Update head
    head = OM::Common.get_head
    outcomes = outcome_list.map do |oc|
      oc.get_sha
    end
    head.step = OM::Types::Progression.new outcomes, title
    OM::Common.write_head head

    puts "Progression Started"
  end

  def help
    puts "Here is where the help text should"
  end
end
