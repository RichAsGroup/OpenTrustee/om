require "dir"
require "file"
require "pretty_print"
require "../../command"
require "../../data_types"
require "../../common"

include OM::Common

class OM::Commands::Outcome::List < OM::Command
  @name = "list"
  @alias = %w(l)
  @use = "displays a list of outcomes"

  def invoke(argv : Array(String))
    puts ""
    puts "Project outcomes ".colorize.mode(:bold).to_s
    puts ""

    # Get outcomes
    outcomes = get_outcomes

    # List out the outcomes
    # puts "Index \t\tName"
    #
    output = [] of String | Array(String)
    output = output.concat print_elements outcomes
    puts align output, delimeter: ""
  end
end
