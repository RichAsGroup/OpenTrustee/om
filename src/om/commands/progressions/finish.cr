require "../../common"
require "../../command"
require "optarg"

include OM::Common

class FinishModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Progression::Finish < OM::Command
  @name = "finish"
  @alias = %w(done d f)
  @use = "End working on a portion of an outcome"

  def initialize
  end

  def invoke(argv : Array(String), command = self)
    result = FinishModel.parse argv
    if result.h? || result.help?
      help
      exit 0
    end

    # Find the current step being worked on
    prog = get_head.step
    if prog.nil?
      puts "No current progression"
      return
    end

    # TODO: This should happen in an editor
    print "Write about how you think you progressed: "
    step_desc = gets.as(String).strip.chomp

    # Commit to object (Caveat: due to yaml and messagepack conflicts outcomes
    # can not be nil)
    duration = Time.utc.to_unix - prog.time_start
    new_prog = OM::Types::Progression.new "", prog.title,
      prog.time_start, step_desc, duration
    hash = write_object "progression", new_prog.to_msgpack

    updated_outcomes = [] of OM::Types::Outcome
    # Change outcomes msgpack
    prog.outcomes.each do |outcome|
      object = get_object outcome
      object = OM::Types::Outcome.from_msgpack object
      object.steps << hash
      # Store the updated outcome
      updated_outcomes << object
    end

    # Create the update outcome
    updated_outcomes.each do |outcome|
      update_outcome outcome
    end

    # Change HEAD
    head = get_head
    head.step = nil
    write_head head
    duration = Time::Span.new(seconds: duration, nanoseconds: 0).to_s
    puts "Progression Completed. Duration #{duration}"
  end

  def help
    puts "Here is the help text"
  end
end
