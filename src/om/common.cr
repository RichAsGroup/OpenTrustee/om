require "dir"
require "yaml"
require "file"
require "digest/sha1"
require "compress/gzip"
require "msgpack"
require "process"
require "io"
require "file_utils"
require "./data_types"

module OM::Common
  extend self

  def project_initialised(do_init = false)
    default_dir = ENV.fetch "PHLEAU_DIR", "./.om"
    # Check for folder
    project_folder = find_project_folder "."
    if !project_folder
      if !do_init
        return false
      end

      project_folder = File.expand_path default_dir
      FileUtils.mkdir_p project_folder
    end

    # Check for outcomes/
    outcomes_folder = File.join project_folder.as(String), "outcomes"
    if !Dir.exists? outcomes_folder
      if !do_init
        return false
      end

      Dir.mkdir outcomes_folder
    end

    # Check for objects/
    objects_folder = File.join project_folder.as(String), "objects"
    if !Dir.exists? objects_folder
      if !do_init
        return false
      end

      Dir.mkdir objects_folder
    end

    # Check for head
    head_file = File.join project_folder.as(String), "HEAD"
    if !File.exists? head_file
      if !do_init
        return false
      end

      # Generate a root tree
      root_tree = generate_tree outcomes_folder

      head = OM::Types::Head.new
      head.tree = root_tree.as(String)

      File.write head_file, head.to_yaml
    end

    # Check for config file
    config_file = File.join project_folder.as(String), ".om"
    if !File.exists? config_file
      if !do_init
        return false
      end
      config = OM::Types::Config.new
      File.write config_file, config.to_yaml
    end

    # Add support for a hooks dir
    hooks_folder = File.join project_folder.as(String), "hooks"
    if !Dir.exists? hooks_folder
      if !do_init
        return false
      end

      Dir.mkdir hooks_folder
    end

    return true
  end

  def find_project_folder(dir : String) : String | Bool
    dir = File.expand_path dir
    foldername = File.basename File.expand_path(ENV.fetch("PHLEAU_DIR", "./.om"))

    phleau_dir = File.join dir, foldername
    if !Dir.exists?(phleau_dir)
      next_dir = File.dirname(dir)
      return false if next_dir == dir
      return find_project_folder(next_dir)
    else
      return phleau_dir
    end
  end

  def find_project_folder
    find_project_folder "."
  end

  def get_outcome_folder
    File.join find_project_folder(".").as(String), "outcomes"
  end

  def get_objects_folder
    File.join find_project_folder(".").as(String), "objects"
  end

  def compute_hash(type : String, content : String | Slice)
    Digest::SHA1.hexdigest "#{type} #{content.size}\0#{content}"
  end

  def write_object(type : String, content : String | Slice)
    hash = compute_hash type, content
    objects_dir = File.join get_objects_folder.as(String), hash[0..1]

    # Make sure the object's subdir exists
    FileUtils.mkdir_p objects_dir

    # Don't store the hash if one already exists
    file = File.join objects_dir, hash[2..-1]
    if File.exists? file
      return hash
    end

    # Write out the outcome object
    File.open(File.join(objects_dir, hash[2..-1]), "w+") do |file|
      Compress::Gzip::Writer.open(file) do |deflate|
        IO.copy IO::Memory.new(content), deflate
      end
      file.close
    end

    hash
  end

  def write_head(head : OM::Types::Head)
    head_file = File.join find_project_folder.as(String), "HEAD"
    File.write head_file, head.to_yaml
  end

  def remove_from_tree(tree_hash : String, name : String) : String
    # Find existing tree
    tree = get_object tree_hash
    # Pull in contents
    tree = OM::Types::Tree.from_msgpack tree
    # Remove outcome from hash
    if tree.objects.has_key? name
      tree.objects.delete name
      # Write new tree
      new_tree_hash = write_object "tree", tree.to_msgpack
      return new_tree_hash
    end
    ""
  end

  def generate_tree(dir : String) : String
    # Check dir exists
    dir = File.expand_path dir
    if !Dir.exists? dir
      return ""
    end

    hashes = {} of String => OM::Types::TreeObject
    # Traverse directory
    Dir.each dir do |filename|
      # Filter unwanted files
      if filename == ".." || filename == "."
        next
      end

      file = File.join dir, filename
      type = "outcome"
      # Check type of file
      if File.file? file
        content = File.read file
        # write_objects for files
        hash = write_object type, content
      else
        # Recursively run generate_tree if dir
        type = "tree"
        hash = generate_tree file
      end

      # add hash to hashes{}
      hashes[filename.as(String)] = OM::Types::TreeObject.new type, hash.as String
    end

    # create tree object
    tree = OM::Types::Tree.new hashes
    # write_object for tree
    write_object "tree", tree.to_msgpack
  end

  def generate_tree : String
    generate_tree(get_outcome_folder.as(String))
  end

  def change_root_tree(hash : String)
    # Open head
    head = get_head
    # New new tree hash
    head.tree = hash
    # save
    write_head head
  end

  def get_head
    head = File.read File.join find_project_folder.as(String), "HEAD"
    OM::Types::Head.from_yaml head
  end

  def get_object(hash : String)
    file = File.join get_objects_folder.as(String), hash[0..1], hash[2..-1]
    if !File.exists? file
      return ""
    end

    string = File.open(file, "r") do |file|
      Compress::Gzip::Reader.open(file) do |inflate|
        inflate.gets_to_end
      end
    end
  end

  def update_tree(previous : String, name : String, object : OM::Types::TreeObject)
    # Open old tree
    tree = get_object previous
    tree = OM::Types::Tree.from_msgpack tree
    # Update tree
    tree.hashes[name] = object

    tree.parent = previous
    # Hash updated tree
    new_tree_hash = write_object "tree", tree.to_msgpack
    # Save
    new_tree_hash
  end

  def create_outcome(name : String, description : String, parents : Array(String) = [] of String)
    outcome = OM::Types::Outcome.new(name: name, description: description, parents: parents)
    pack = outcome.to_msgpack
    outcome_dir = File.join get_outcome_folder.as(String)

    # Write out the outcome file
    pack = outcome.to_msgpack
    filepath = outcome.parents
    filepath.unshift get_outcome_folder.as(String)
    filepath << "#{outcome.name}.mpck"
    path = File.join(filepath)
    FileUtils.mkdir_p File.dirname path
    File.write path, IO::Memory.new pack

    # open head
    tree_hash = generate_tree
    change_root_tree tree_hash
  end

  def update_outcome(outcome : OM::Types::Outcome, previous_name : String = "")
    if previous_name != ""
      # TODO: Implement when needed
    end

    pack = outcome.to_msgpack
    filepath = outcome.parents
    filepath.unshift get_outcome_folder.as(String)
    filepath << "#{outcome.name}.mpck"
    path = File.join(filepath)
    FileUtils.mkdir_p File.dirname path
    File.write File.join(filepath), IO::Memory.new pack

    tree_hash = generate_tree
    change_root_tree tree_hash
  end

  ##################################################################################
  # Code for alignment in the command line
  # https://github.com/mrrooijen/commander/blob/master/src/commander/command.cr#L163

  def get_version : String
    "0.9.5"
  end

  def get_user : String
    user = ENV.fetch("USER", 0)
    unless user == 0
      # ENV["%USERNAME%"] for windows(i think)
      raise Exception.new("Could not determine user")
    end
    user
  end

  def get_editor : String
    # Valid Editor Chain: PHLEAU_EDITOR > EDITOR > GIT_EDITOR
    ENV.fetch "PHLEAU_EDITOR", ENV.fetch "EDITOR", ENV.fetch "GIT_EDITOR", "vi"
  end

  def launch_editor(filename : String) : Tuple
    {Int, String}
    editor = get_editor
    result = ""
    proc = Process.new(
      editor, # command
 args: [filename],
      shell: true,
      input: STDIN,
      output: STDOUT,
      error: STDERR
    )

    status = proc.wait
    if status.normal_exit?
      result = File.read(filename)
    end

    {status.exit_code, result}
  end

  def iterate_tree(tree : OM::Types::Tree, from_outcome : String = "", flat : Bool = false) : Array(OM::Types::Outcome)
    outcome_tree = {} of String => Array(OM::Types::Outcome)
    outcome_array = [] of OM::Types::Outcome

    # Start by Recursively parsing the tree objects
    tree.objects.each_key do |filename|
      object = tree.objects[filename]
      pack = get_object object.sha
      if object.type == "tree"
        new_tree = OM::Types::Tree.from_msgpack pack
        outcome_tree[filename] = iterate_tree(new_tree, from_outcome, flat)
        next
      elsif object.type == "outcome"
        new_outcome = OM::Types::Outcome.from_msgpack pack
        outcome_array << new_outcome
        next
      end
    end

    flat_array = [] of OM::Types::Outcome
    outcome_array.map do |oc|
      children = [] of OM::Types::Outcome
      if outcome_tree[oc.name]?
        children = outcome_tree[oc.name]
        oc.children = children
      end

      if flat
        flat_array << oc
        flat_array = flat_array.concat children
      end

      oc
    end

    if flat
      flat_array
    else
      outcome_array
    end
  end

  def print_elements(elements : Array(OM::Types::Outcome), indent : Int32 = 0, initial_index : Int32 = 0, selected : Array(OM::Types::Outcome) = [] of OM::Types::Outcome) : Array(String | Array(String))
    padding = "  "
    output = [] of String | Array(String)
    elements.each_index do |index|
      item = elements[index]
      pad = padding*indent
      if pad.size >= 2
        output << ["", "#{pad}|".colorize(:cyan).to_s]
        pad = pad + "+—"
      end
      prefix = " "
      selected.each do |selection|
        if item.get_sha == selection.get_sha
          prefix = "*"
        end
      end
      output << ["#{prefix}#{index + 1 + initial_index}:", "#{pad}#{item.name}".colorize(:cyan).to_s]
      if item.children.size > 0
        initial_index += 1
        n = print_elements(item.children, indent + 1, index + initial_index, selected)
        output = output.concat n
        next
      end
    end
    output
  end

  def get_outcomes(from_outcome : String = "", flat : Bool = false) : Array(OM::Types::Outcome)
    head = get_head
    tree = get_object head.tree
    tree = OM::Types::Tree.from_msgpack tree
    iterate_tree(tree, from_outcome, flat)
  end

  def align(pair : Array(OM::Types::NestedStringArray), delimeter = " -")
    max_size = pair.reduce(0) do |max, entry|
      if entry.is_a? Array
        size = entry[0].size
      elsif size = entry.size
      end
      size > max ? size : max
    end

    entries = pair.map do |entry|
      # If there's only one string provided
      if entry.is_a? Array
        if entry.size < 2
          next entry[0]
        end

        padding = max_size - entry[0].size
        # If there's more than 2 columns, lets make sure they are aligned too
        if entry.size > 2
          next "#{entry[0]}#{" " * padding} #{delimeter} #{align([entry[1, entry.size]], delimeter)}"
        else
          next_set = entry[1]
          if next_set.is_a? Array
            next "#{entry[0]}#{" " * padding} #{delimeter} #{align(next_set, delimeter)}"
          end
        end

        "#{entry[0]}#{" " * padding} #{delimeter} #{entry[1]}"
      else
        next entry
      end
    end

    entries.join("\n")
  end
end
