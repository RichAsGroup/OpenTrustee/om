require "../commands"
require "../common"
require "../data_types"
require "./progression"
require "optarg"
require "time"
require "colorize"

include OM::Common

class StatusModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Status < OM::Command
  @name = "status"
  @alias = %w(s)
  @use = "Display current status of project"

  def initialize : Nil
  end

  def invoke(argv : Array(String), command = self) : Nil
    result = StatusModel.parse argv
    if result.h? || result.help?
      help
      exit 0
    end

    # Get information from the head
    head = get_head

    # Display project
    puts "\n"
    puts "Project Status\n".colorize.mode(:bold).mode(:underline).to_s
    output = [] of String | Array(String)
    initialised = head.tree != ""
    output << ["Initialised:", "#{initialised.to_s.capitalize}"]
    output << [""]

    if initialised # Output other project information
      outcomes = OM::Common.get_outcomes("", flat: true)
      top_outcomes = OM::Common.get_outcomes
      output << ["Root Outcomes:", "#{top_outcomes.size}"]
      output << ["Total Outcomes:", "#{outcomes.size}"]

      # Output the tree information (latest progressions)
      # tree = OM::Types::Tree.from_msgpack get_object(head.tree)

    else # If no information can be found

      puts "No project status could be found. After more activity we will have more to show you"
    end

    # Get the step content
    output << [""]
    progression = OM::Commands::Progression.progression_information.map_with_index do |line, index|
      if line.is_a? Array
        if index == 0
          next line.unshift("Current Progression:")
        end
        next line.unshift("")
      end
      line
    end

    output = output + progression
    puts align(output, delimeter = "  ")
  end

  def help : Nil
    puts "Display status of project"
  end
end
