require "../command"
require "../common"
require "optarg"

class OM::Commands::Init < OM::Command
  @name = "init"
  @alias = %w(initialise initialize i)
  @use = "Initialises OM for a particular project"
  @needs_init = false

  def initialize
  end

  def invoke(argv : Array(String), command = self)
    if OM::Common.project_initialised false
      puts "Project already initialised"
      exit 0
    end

    OM::Common.project_initialised true
    puts "Project initialised"
  end
end
