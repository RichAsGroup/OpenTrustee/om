class OM::Command
  @name = ""
  @use = ""
  @alias = [] of String

  @needs_init = true
  getter :name, :use, :needs_init, :alias
  setter :name, :use

  def initialise
  end

  def invoke(argv : Array(String))
    raise "Not Implemented"
  end
end
