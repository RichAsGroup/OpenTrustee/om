require "../command"
require "../common"
require "optarg"

class VersionModel < Optarg::Model
  bool %w(-h --help), default: false
end

class OM::Commands::Version < OM::Command
  @name = "version"
  @use = "Return the version of OM"
  @alias = %w(version v)
  @needs_init = false

  def initialize
  end

  def invoke(argv : Array(String), command = self)
    puts "OM version #{OM::Common.get_version}"
  end
end
